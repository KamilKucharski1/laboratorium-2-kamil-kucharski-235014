tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (e=expr {print ($e.text + " = " + $e.out.toString());}) |d=declare)*;

declare
    :
      ^(VAR id=ID) {createSymbol($id.text);}
    ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(MOD   e1=expr e2=expr) {$out = $e1.out \% $e2.out;}
        | ^(POT   e1=expr e2=expr) {$out = (int)Math.pow($e1.out, $e2.out); }
        | ^(PODST id=ID   e2=expr) {assignSymbol($id.text, $e2.out); $out = returnSymbol($id.text);}
        | ID                       {$out = returnSymbol($ID.text);   }
        | INT                      {$out = getInt($INT.text);}
        ;
