/**
 * 
 */
package tb.antlr.kompilator;

import java.util.HashMap;
import java.util.Iterator;

import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.TreeNodeStream;
import org.antlr.runtime.tree.TreeParser;

import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols;

/**
 * @author tb
 *
 */
public class TreeParserTmpl extends TreeParser {

	protected LocalSymbols localSymbols = new LocalSymbols();
	
	/**
	 * @param input
	 */
	public TreeParserTmpl(TreeNodeStream input) {
		super(input);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param input
	 * @param state
	 */
	public TreeParserTmpl(TreeNodeStream input, RecognizerSharedState state) {
		super(input, state);
		// TODO Auto-generated constructor stub
	}

	protected void errorID(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}
	
	protected void enterScope() 
	{
		localSymbols.enterScope();
	}

	protected void leaveScope() 
	{
		localSymbols.leaveScope();
	}

	protected void setSymbol(String name, Integer value) 
	{
		localSymbols.setSymbol(name, value);
	}

	protected Integer getLocalVarName(String name) 
	{
		return localSymbols.getScopeVariable(name);
	}
}
