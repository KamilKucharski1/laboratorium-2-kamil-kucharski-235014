tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | e+=blockScope | d+=decl)* -> start(name={$e},deklaracje={$d})
        ;

blockScope   : ^(LS {localSymbols.enterScope();} (e+=blockScope | e+=expr | d+=decl)* {localSymbols.leaveScope();}) -> blockScope(name={$e},deklaracje={$d})
        ;

decl  :
        ^(VAR i1=ID) {localSymbols.newSymbol($i1.text);} -> createVar(name={$i1.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(var1={$e1.st},var2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> subtract(var1={$e1.st},var2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(var1={$e1.st},var2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(var1={$e1.st},var2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {localSymbols.getSymbol($ID.text);} -> setVar(name={$i1.text +" scope: "+getLocalVarName($i1.text)},value={$e2.st})
        | INT                      -> int(i={$INT.text})
        | ID {localSymbols.getSymbol($ID.text);} -> getVar(name={$ID.text +" scope: "+getLocalVarName($i1.text)})
    ;
    