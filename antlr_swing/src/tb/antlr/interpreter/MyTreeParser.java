package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols.*;

public class MyTreeParser extends TreeParser {
	tb.antlr.symbolTable.GlobalSymbols globals = new tb.antlr.symbolTable.GlobalSymbols();
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected void createSymbol(String text) {
		globals.newSymbol(text);
	}
	
	protected void assignSymbol(String text, Integer value) {
		globals.setSymbol(text, value);
	}
	
	protected Integer returnSymbol(String text) {
		return globals.getSymbol(text);
	}
}
